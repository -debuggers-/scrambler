﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scrambler
{
    public class TezaursUrlSet : Locations
    {
        public override void Add(string loc)
        {
            loc = loc.Replace(ConfigHelper.ToReplace, ConfigHelper.ReplaceWith);
            base.Add(loc);
        }
    }
}
