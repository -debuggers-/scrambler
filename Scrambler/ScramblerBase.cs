﻿using HtmlAgilityPack;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Net;
using System;

namespace Scrambler
{
    public abstract class ScramblerBase : ITypeScrambler
    {
        public abstract void BeforeScramble();
        public abstract void AfterScramble();
        public abstract void OnNameReaded(Name name);
        private static int i = 0;

        private Name ReadWord(HtmlDocument doc)
        {
            try
            {
                var head = doc.DocumentNode.SelectNodes("//*[@class=\"sv_Head\"]");

                if (head.Count != 1)
                    throw new ScramblerException("Could not find 'sv_Head' class element!");

                HtmlNode element = head[0];
                var word = element.SelectNodes("//*[@class=\"sv_VR\"]");

                var name = new Name()
                {
                    Value = Encode(word[0].InnerText)
                };

                // Tries to get idioms, if any exists
                var idioms = doc.DocumentNode
                    .SelectNodes("//div[@class='sv_Idioms']/div[@class='sv_Items']/div");

                if (idioms != null)
                {
                    foreach (var idiom in idioms)
                    {
                        string title = Encode(idiom.SelectSingleNode("./span[@class='sv_FR']").InnerText);
                        string explanation = Encode(idiom.SelectSingleNode("./span[@class='sv_FN']").InnerText);

                        name.Idioms.Add(new Idiom() { Title = title, Explanation = explanation });
                    }
                }

                return name;
            }
            catch (System.Exception e)
            {
                throw new ScramblerException(e.Message);
            }

        }


        public void Scramble(TezaursUrlSet urls)
        {
            this.BeforeScramble();
            foreach (var url in urls.Get())
            {
                try
                {
                    var request = (HttpWebRequest)WebRequest.Create(url);
                    var doc = new HtmlDocument();

                    using (WebResponse response = request.GetResponse())
                    {
                        using (Stream stream = response.GetResponseStream())
                        {
                            doc.Load(stream);
                        }
                    }

                    Console.WriteLine(String.Format("{0}. {1}", ++i, url));
                    OnNameReaded(ReadWord(doc));

                }
                catch (ScramblerException e)
                {
                    Console.WriteLine(e.Message);

                    var error = new StringBuilder();

                    error.AppendLine();
                    error.AppendLine("------------------------------");
                    error.AppendLine();
                    error.AppendFormat("[{0}]", DateTime.Now);
                    error.AppendLine();
                    error.AppendFormat("URL: {0}", url);
                    error.AppendLine();
                    error.AppendFormat("Message: {0}", e.Message);

                    File.AppendAllText(ConfigHelper.ScramblingLogFile, error.ToString());
                }
                catch (Exception e)
                {
                    Console.WriteLine(String.Format("Critical: {0}", e.Message));

                    var error = new StringBuilder();

                    error.AppendLine();
                    error.AppendLine("------------------------------");
                    error.AppendLine();
                    error.AppendFormat("[{0}]", DateTime.Now);
                    error.AppendLine();
                    error.AppendFormat("URL: {0}", url);
                    error.AppendLine();
                    error.AppendFormat("Message: {0}", e.Message);
                    error.AppendLine();
                    error.AppendFormat("Trace: {0}", e.StackTrace);

                    File.AppendAllText(ConfigHelper.ErrorLogFile, error.ToString());
                }
            }
            this.AfterScramble();
        }

        public static string Encode(string str)
        {
            byte[] bytes = Encoding.Default.GetBytes(str);

            return Encoding.UTF8.GetString(bytes);
        }
    }
}
