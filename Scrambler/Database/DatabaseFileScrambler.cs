﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scrambler
{
    public class DatabaseFileScrambler : ScramblerBase, ITypeScrambler
    {
        public override void BeforeScramble()
        {
            if (File.Exists(ConfigHelper.OutputName))
            {
                File.Delete(ConfigHelper.OutputName);
            }

            //File.Create(ConfigHelper.OutputName);
            var prefix = File.ReadAllText("Database\\PrefixContent.txt");
            File.AppendAllText(ConfigHelper.OutputName, prefix);
        }

        public override void AfterScramble()
        {

        }

        public override void OnNameReaded(Name name)
        {
            var sql = new StringBuilder();
            sql.AppendLine();
            sql.AppendFormat(@"INSERT INTO `words` (`word`, `type`) VALUES('{0}', '{1}');", name.Value, name.Type);
            if (name.Idioms.Count > 0)
            {
                sql.AppendLine();
                sql.AppendLine("SET @last_id = LAST_INSERT_ID();");
                foreach (var idiom in name.Idioms)
                {
                    sql.AppendLine();
                    sql.AppendFormat("INSERT INTO `idioms` (`word_id`, `title`, `explanation`) VALUES(@last_id, '{0}', '{1}');",
                        idiom.Title, idiom.Explanation);
                }
            }

            File.AppendAllText(ConfigHelper.OutputName, sql.ToString());
        }
    }
}
