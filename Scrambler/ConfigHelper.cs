﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scrambler
{
    public class ConfigHelper
    {
        public static string ReplaceWith
        {
            get
            {
                return ConfigurationManager.AppSettings["replace-with"];
            }
        }

        public static string ToReplace
        {
            get
            {
                return ConfigurationManager.AppSettings["to-replace"];
            }
        }

        public static string SitemapUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["sitemap-url"];
            }
        }

        public static int WordCount
        {
            get
            {
                int result = 0;

                int.TryParse(ConfigurationManager.AppSettings["word-count"], out result);

                return result;
            }
        }

        public static string OutputName
        {
            get
            {
                return ConfigurationManager.AppSettings["output-name"];
            }
        }

        public static OutputType OutputType
        {
            get
            {
                OutputType result = OutputType.File;
                var confValue = ConfigurationManager.AppSettings["output-type"];

                Enum.TryParse<OutputType>(confValue, true, out result);

                return result;
            }
        }

        public static string ScramblingLogFile
        {
            get
            {
                return ConfigurationManager.AppSettings["scrambling-log"];
            }
        }

        public static string ErrorLogFile
        {
            get
            {
                return ConfigurationManager.AppSettings["error-log"];
            }
        }

        public static string WordTypeSource
        {
            get
            {
                return ConfigurationManager.AppSettings["word-type-source"];
            }
        }
    }
}
