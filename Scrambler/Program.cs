﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml;

namespace Scrambler
{
    class Program
    {
        static void Main(string[] args)
        {
            var scrambler = new Scrambler();


            DateTime start = DateTime.Now;

            scrambler.Exec();

            DateTime end = DateTime.Now;

            // Difference
            TimeSpan difference = end - start;

            // Output
            Console.Out.WriteLine("Start      Time: " + start.ToString("hh:mm:ss:fff"));
            Console.Out.WriteLine("End        Time: " + end.ToString("hh:mm:ss:fff"));
            Console.Out.WriteLine("Completion Time: " + difference.ToString(@"hh\:mm\:ss\:fff"));

            Console.ReadLine();
        }
    }
}
