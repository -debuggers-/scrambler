﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scrambler
{
    public class Locations
    {
        private IList<string> locations = new List<string>();

        /// <summary>
        /// Add new item
        /// </summary>
        /// <param name="loc">New location string object</param>
        public virtual void Add(string loc)
        {
            locations.Add(loc);
        }

        /// <summary>
        /// Get locations
        /// </summary>
        /// <returns>List of locations</returns>
        public IList<string> Get()
        {
            return locations;
        }

        public int Count
        {
            get
            {
                return locations.Count;
            }
        }
    }
}
