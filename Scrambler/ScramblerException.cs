﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scrambler
{
    public class ScramblerException : Exception
    {
        public ScramblerException(string message) : base(message) { }
    }
}
