﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scrambler
{
    public static class HtmlDocumentExtensions
    {

        public static Name getScramblerName(this HtmlDocument doc)
        {
            var name = new Name();

            name.Value = doc.DocumentNode.SelectSingleNode("//body//div//div//span[@class='sv_VR']").InnerHtml;

            return name;
        }

    }
}

