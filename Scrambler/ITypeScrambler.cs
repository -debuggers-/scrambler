﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scrambler
{
    public interface ITypeScrambler
    {
        void BeforeScramble();
        void Scramble(TezaursUrlSet urls);
        void OnNameReaded(Name name);
        void AfterScramble();
    }
}
