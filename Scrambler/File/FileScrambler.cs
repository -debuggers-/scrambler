﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using HtmlAgilityPack;

namespace Scrambler
{
    public class FileScrambler : ScramblerBase, ITypeScrambler
    {

        public override void BeforeScramble()
        {
            if (File.Exists(ConfigHelper.OutputName))
            {
                File.Delete(ConfigHelper.OutputName);
            }
        }

        public override void AfterScramble()
        {

        }

        public override void  OnNameReaded(Name name)
        {
            var text = String.Format("{0} {1} {2}", name.Value, name.Type, Environment.NewLine);
            File.AppendAllText(ConfigHelper.OutputName, text);
        }
    }
}
