﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scrambler
{
    public enum OutputType
    {
        File = 0,
        Database = 1,
        DatabaseFile = 2
    }
}
