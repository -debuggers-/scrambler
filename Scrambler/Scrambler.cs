﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Scrambler
{
    public class MyWebClient : WebClient
    {
        protected override WebRequest GetWebRequest(Uri uri)
        {
            WebRequest w = base.GetWebRequest(uri);
            w.Timeout = 1000;
            return w;
        }
    }

    public class Scrambler
    {
        public static string ScrambleType(string word)
        {
            try
            {
                using (var wc = new MyWebClient())
                {
                    var url = String.Format("{0}{1}", ConfigHelper.WordTypeSource, word);
                    var json = ScramblerBase.Encode(wc.DownloadString(url));
                    json = json.Substring(json.IndexOf("Vārdšķira"));
                    var end = json.IndexOf("\",");
                    if (end < 0)
                        end = json.IndexOf("\"}");

                    json = json.Substring(0, end);

                    return json.Replace("Vārdšķira\":\"", "");
                }
            }
            catch (Exception e)
            {
                return String.Empty;
            }
        }

        public void Exec()
        {
            var sitemap = Load();
            ITypeScrambler scrambler;

            switch (ConfigHelper.OutputType)
            {
                case OutputType.DatabaseFile:
                    scrambler = new DatabaseFileScrambler();
                    break;
                default:
                    scrambler = new FileScrambler();
                    break;
            }

            var urlSet = new TezaursUrlSet();

            foreach (var endpoint in sitemap.Get())
            {
                var doc = Load(endpoint);

                foreach (XmlNode xnode in doc["urlset"])
                {
                    string name = xnode.FirstChild.InnerText;

                    urlSet.Add(name);

                    if (urlSet.Count >= ConfigHelper.WordCount && urlSet.Count != 0)
                    {
                        scrambler.Scramble(urlSet);
                        return;
                    }
                }
            }

            scrambler.Scramble(urlSet);
        }


        private TezaursSiteMap Load()
        {
            var sitemap = new TezaursSiteMap();

            string sitemapUrl = ConfigHelper.SitemapUrl;

            var doc = Load(sitemapUrl);

            foreach (XmlNode xnode in doc["sitemapindex"])
            {
                string name = xnode.FirstChild.InnerText;

                sitemap.Add(name);
            }

            return sitemap;
        }

        private XmlDocument Load(string url)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);

            var doc = new XmlDocument();

            using (WebResponse response = request.GetResponse())
            {
                using (Stream stream = response.GetResponseStream())
                {
                    doc.Load(stream);
                }
            }

            return doc;
        }
    }
}
