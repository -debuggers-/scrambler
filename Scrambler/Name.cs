﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scrambler
{
    public class Name
    {
        private string value;
        public List<Idiom> Idioms = new List<Idiom>();

        public string Value
        {
            get { return value; }
            set
            {
                this.value = value;
                this.Type = Scrambler.ScrambleType(value);
            }
        }

        public string Type { get; set; }
    }

    public class Idiom
    {
        public string Title { get; set; }
        public string Explanation { get; set; }
    }
}
